<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo base_url(); ?>assets/ico/favicon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
 
    <link href="<?php echo base_url(); ?>vendor/almasaeed2010/adminlte/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>vendor/almasaeed2010/adminlte/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    
    </head>
    <body class="bg-black">
    <div class="form-box" id="login-box">
      <div class="header"><img src="<?php echo base_url(); ?>assets/img/codeigniter.png" alt="Logo"></div>
       <form class="form-horizontal" role="form" method="POST" action="">
            <div class="body bg-gray">
                <div class="form-group">
                    <input id="email" type="text" name="email" class="form-control" placeholder="Username" value="" required autofocus/>
                </div>
                <div class="form-group">
                    <input id="password" type="password" name="password" class="form-control" placeholder="Password" required />
                </div>
            </div>
            <div class="footer">
               <button type="submit" class="btn  btn-block btn-black">Sign me in</button>
            </div>
        </form>
    </div>
    </body>
</html>
